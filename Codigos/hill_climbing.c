#include "hill_climbing.h"


solucao hill_climbing(){
  int i;
  int cont = 0;
  solucao solucao_1;
  solucao S_ajustada;
  int tentativas = 0;


  solucao_1 = solucao_inicial();

  do {
    S_ajustada = ajuste(solucao_1);

    if (custo_2(S_ajustada) < custo_2(solucao_1)) {
      cont = 0;
      for (i = 0; i < tamanho; i++) {
        solucao_1.vetor[i] = S_ajustada.vetor[i];
      }
    }else{
      cont++;
    }
    tentativas++;
  } while((cont < tentativas_sem_melhora) & (tentativas < parada));

  printf("N funcoes testadas: %d -> ",tentativas);
  return solucao_1;
}

solucao steepest_ascent_hill_climbing(){
  int i;
  int cont = 0;
  int tentativas = 0;
  solucao solucao_1;
  solucao S_ajustada_1;
  solucao S_ajustada_2;

  solucao_1 = solucao_inicial();

  do {
    S_ajustada_1 = ajuste(solucao_1);
    for (i = 0; i < vizinhos_SAHC; i++) {
      S_ajustada_2 =  ajuste(solucao_1);
      if (custo_2(S_ajustada_2) < custo_2(S_ajustada_1)) {
        for (i = 0; i < tamanho; i++) {
          S_ajustada_1.vetor[i] = S_ajustada_2.vetor[i];
        }
      }
    }

    if (custo_2(S_ajustada_1) < custo_2(solucao_1)) {
      cont = 0;
      for (i = 0; i < tamanho; i++) {
        solucao_1.vetor[i] = S_ajustada_1.vetor[i];
      }
    }else{
      cont++;
    }

    tentativas++;
  } while((cont < tentativas_sem_melhora) & (tentativas < parada));

  printf("N funcoes testadas: %d -> ",tentativas);
  return solucao_1;
}


//////////////////////////////// FUNÇÕES AUXILIARES ////////////////////////////////

solucao solucao_inicial(){
  int i;
  float valor_aleatorio;
  solucao s;

  for ( i = 0; i < tamanho; i++) {
    valor_aleatorio = aleatorio (); // valor aleatorio entre 0 e 1
    s.vetor[i] = (limite_superior[i] - limite_inferior[i]) * valor_aleatorio + limite_inferior[i] ;
  }
  return s;
}

solucao ajuste(solucao solucao_1){
  solucao valor_ajustado;
  int i;

  for (i = 0; i < tamanho; i++) {
    if (p >= aleatorio ()) {
      do {
        valor_ajustado.vetor[i]  =  solucao_1.vetor[i] + tamanho_pertubacao();// tamanho_pertubacao -> numero aleatorio entre -pertubacao e pertubacao
      } while( !( (valor_ajustado.vetor[i] >= limite_inferior[i]) & (valor_ajustado.vetor[i] <= limite_superior[i]) ) );
    }
  }

  return valor_ajustado;
}

// numero aleatorio entre -pertubacao e pertubacao
float tamanho_pertubacao(){
  float r, valor_aleatorio;

  valor_aleatorio = aleatorio (); // numero de 0 a 1

  r = pertubacao * valor_aleatorio; // valor aleatorio positivo menor ou igual ao da pertubacao
  if (aleatorio () < 0.5) {
    r = (-1) * pertubacao * valor_aleatorio; // valor aleatorio negativo maior ou igual ao da pertubacao
  }
  return r;
}

// numero aleatorio de 0 a 1
float aleatorio (){
  return (float)rand() / (float) RAND_MAX;
}

void limites(){
  int i;
  for ( i = 0; i < tamanho; i++) {
    do {
      printf("Limite inferior variavel %d: ", i+1);
      scanf("%f", &limite_inferior[i]);
      printf("Limite superior variavel %d: ", i+1);
      scanf("%f", &limite_superior[i]);
      if (limite_inferior[i] > limite_superior[i]) {
        system("clear");// limpar a tela
        printf("O Limite inferior é maior que o Limite Superior.\n");
      }
    } while(limite_inferior[i] > limite_superior[i]);// garantir que o limite inferior sera menor ou igual ao limite superior
  }
  system("clear");// limpar a tela
}

float custo_1(solucao variavel){
  return variavel.vetor[0]* variavel.vetor[0];
}

float custo_2(solucao variavel){
  // variavel.vetor[0] = x; variavel.vetor[1] = y
  return (-(variavel.vetor[1] + 47) * sin( sqrt( abs((variavel.vetor[0]/2) + (variavel.vetor[1] + 47) ) ) ) ) -
         (variavel.vetor[0] * sin( sqrt( abs( variavel.vetor[0] - (variavel.vetor[1] + 47) ) ) ));
}
