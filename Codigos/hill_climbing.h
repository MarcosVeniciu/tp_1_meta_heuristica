#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

#define tamanho 2 // numero de variaveis
#define parada 4000 // numero de iteracoes
#define tentativas_sem_melhora 20
#define pertubacao 0.5 // tamanho da pertubação
#define p 1   //probabilidade de pertubação
#define vizinhos_SAHC 10 // numero de visinhos testados

typedef struct{
  float vetor[tamanho];
}solucao;
float limite_inferior[tamanho];
float limite_superior[tamanho];



solucao hill_climbing();

solucao steepest_ascent_hill_climbing();

// Funções Secundarias
float custo_1(solucao variavel);
float custo_2(solucao variavel);
solucao solucao_inicial();
solucao ajuste (solucao solucao);
float tamanho_pertubacao();
void limites();

// Funçoes Auxiliares
float aleatorio ();
