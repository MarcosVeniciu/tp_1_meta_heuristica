#include "hill_climbing.h"

// pra compilar direito gcc main.c hill_climbing.c -lm


int main() {
  int i = 0;
  solucao final;
  time_t temp;
  float custo_final;
  float resultados[100];
  float media = 0;
  float minimo;
  float maximo;
  float desvio_padrao = 0;
  float quantidade = 30;




  srand( (unsigned)time(&temp) );
  limites();

  // primeira execução
    // final = hill_climbing();
    final = steepest_ascent_hill_climbing();

    custo_final = custo_2(final);
    printf("1 - %f\n", custo_final);

    resultados[0] = custo_final;
    minimo = custo_final; // o menor valor ate agora
    maximo = custo_final; // o maior valor ate agora

  // as outras 29 execuçoes
    for ( i = 1; i < quantidade; i++) {
      // final = hill_climbing();
      final = steepest_ascent_hill_climbing();

      custo_final = custo_2(final);
      printf("%d - %f\n",(i+1), custo_final);



      ////// valores
      resultados[i] = custo_final;
      ////// MINIMO
      if (custo_final < minimo) {
        minimo = custo_final;
      }
      ////// MAXIMO
      if (custo_final > maximo) {
        maximo = custo_final;
      }
    }

    ////// MEDIA
      for (i = 0; i < quantidade; i++) {
        media = media + resultados[0];
      }
      media = media/quantidade;

    ////// DESVIO-PADRAO
     for ( i = 0; i < quantidade; i++) {
      desvio_padrao = desvio_padrao + pow( (resultados[i] - media), 2);
     }
     desvio_padrao = sqrt( desvio_padrao/quantidade );

    printf("\n\n\n");
    printf("Media: %f \n", media);
    printf("Minimo: %f \n", minimo);
    printf("Maximo: %f \n", maximo);
    printf("DESVIO PADRAO: %f \n", desvio_padrao);
  return 0;
}
